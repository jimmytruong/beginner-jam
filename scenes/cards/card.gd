extends Node2D


@onready var area_2d = $Area2D
@onready var sprite = $Sprite2D

func _ready():
	area_2d.mouse_entered.connect(_on_area_2d_mouse_entered)
	area_2d.mouse_exited.connect(_on_area_2d_mouse_exited)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _on_area_2d_mouse_entered():
	sprite.scale = Vector2(1.35, 2.0)
	area_2d.scale = Vector2(1.35, 2.0)


func _on_area_2d_mouse_exited():
	sprite.scale = Vector2(1.0, 1.5)
	area_2d.scale = Vector2(1.0, 1.5)

