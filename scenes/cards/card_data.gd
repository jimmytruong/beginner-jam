extends Resource
class_name CardData
	
@export var name: String = ""
@export var description: String = ""
@export var image: Texture
@export var cost: int = 0
@export var card_type: Dictionary = {
	"attack": false,
	"defense": false,
	"support": false,
	"special": false
}

@export var attack: int = 0
@export var defense: int = 0
@export var special: int = 0
