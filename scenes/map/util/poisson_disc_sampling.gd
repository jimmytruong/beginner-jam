class_name PoissonDiscSampling

func generate_points(radius :float, sampleRegionSize:Vector2, numSamplesBeforeRejection = 30): 
	var cellSize = radius / sqrt(2);
	var grid = _instantiate_grid(ceil(sampleRegionSize.x/cellSize), ceil(sampleRegionSize.y/cellSize))
	var points = [] # List of Vector2s
	var spawnPoints = [] # List of Vector2s
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	spawnPoints.append(sampleRegionSize/2)
	while(len(spawnPoints) > 0):
		var spawnIndex = int(rng.randf_range(0, len(spawnPoints)))
		var spawnCentre = spawnPoints[spawnIndex]
		var candidateAccepted = false
		
		for i in numSamplesBeforeRejection:
			# 0 < 2pi
			var angle = rng.randf() * PI * 2;
			var dir = Vector2(sin(angle), cos(angle))
			var candidate = spawnCentre + dir * rng.randf_range(radius, 2*radius)
			if (_candidate_is_valid(candidate, sampleRegionSize, cellSize, radius, points, grid)):
				points.append(candidate)
				spawnPoints.append(candidate)
				grid[int(candidate.x/cellSize)][int(candidate.y/cellSize)] = len(points)
				candidateAccepted = true
				break
		if !candidateAccepted:
			spawnPoints.remove_at(spawnIndex)
		
	return points;

func _instantiate_grid(x_length:int, y_length:int):
	var grid = []
	for x in range(x_length):
		grid.append([])
		for y in range(y_length):
			grid[x].append(0)
	return grid
	
func _candidate_is_valid(candidate : Vector2, sampleRegionSize: Vector2, cellSize: float, radius: float, points, grid) -> bool:
	if (candidate.x >= 0 && candidate.x < sampleRegionSize.x && candidate.y >= 0 && candidate.y < sampleRegionSize.y):
		var cellX = int(candidate.x / cellSize)
		var cellY = int(candidate.y / cellSize)
		var searchStartX = max(0, cellX-2)
		var searchEndX = min(cellX+2,len(grid) - 1 )
		var searchStartY = max(0, cellY-2)
		var searchEndY = min(cellY+2,len(grid[0]) - 1 )
		
		for x in range(searchStartX, searchEndX+1):
			for y in range(searchStartY, searchEndY+1):
				var pointIndex = grid[x][y] - 1
				if (pointIndex != -1):
					var sqrDst = (candidate - points[pointIndex]).length_squared()
					if (sqrDst < radius * radius):
						return false
		return true
		
		
	return false
