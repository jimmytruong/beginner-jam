@tool
extends Node2D

var changed = false
const POISSON = preload("res://scenes/map/util/poisson_disc_sampling.gd")
var poisson = POISSON.new()
@export var radius: float = 1:
	get:
		return radius
	set(value):
		changed = true
		radius = value
@export var regionSize: Vector2 = Vector2.ONE:
	get:
		return regionSize
	set(value):
		changed = true
		regionSize = value
@export var rejectionSamples: int = 30:
	get:
		return rejectionSamples
	set(value):
		changed = true
		rejectionSamples = value
@export var displayRadius: float = 1:
	get:
		return displayRadius
	set(value):
		changed = true
		displayRadius = value
var color = Color(0.941176, 1, 1, 1)

var points # List of Vector2
# Called when the node enters the scene tree for the first time.
func _ready():
	points = poisson.generate_points(radius, regionSize, rejectionSamples)

	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if changed:
		points = poisson.generate_points(radius, regionSize, rejectionSamples)

func _draw():
	if points:
		for point in points:
			draw_circle(point,displayRadius, color)
